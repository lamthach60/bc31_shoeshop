import React, { Component } from "react";

export default class TableShoe extends Component {
  renderContent = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
            <span className="mx-5">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-success"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              class="btn btn-primary"
            >
              Xóa
            </button>
          </td>
          <td>
            <button class="btn btn-primary">Sửa</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="table ">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.renderContent()}</tbody>
      </div>
    );
  }
}
