import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    let { name, description, image } = this.props.data;
    return (
      <div className="col-4">
        <div className="card" style={{ width: "100%" }}>
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              {description.length < 30
                ? description
                : description.slice(0, 30) + "..."}
            </p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              class="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
