import React, { Component } from "react";
import { shoeArr } from "./dataShoes";
import ShoeItem from "./ShoeItem";
import TableShoe from "./TableShoe";
export default class ExerciseShoe extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  renderShoes = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ShoeItem
          handleAddToCart={this.handleAddToCart}
          data={item}
          key={item.id}
        />
      );
    });
  };
  handleAddToCart = (sp) => {
    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (viTri == -1) {
      let newSP = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSP);
    } else {
      cloneGioHang[viTri].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  handleRemove = (id) => {
    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == id;
    });
    if (viTri !== -1) {
      let cloneGH = [...this.state.gioHang];
      cloneGH.splice(viTri, 1);
      this.setState({ gioHang: cloneGH });
    }
  };
  handleChangeQuantity = (id, step) => {
    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == id;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[viTri].soLuong += step;
    if (cloneGioHang[viTri].soLuong == 0) {
      cloneGioHang.splice(viTri, 1);
    }
    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    return (
      <>
        <div className="container py-5">
          {this.state.gioHang.length > 0 && (
            <TableShoe
              handleRemove={this.handleRemove}
              handleChangeQuantity={this.handleChangeQuantity}
              gioHang={this.state.gioHang}
            />
          )}
          <div className="row">{this.renderShoes()}</div>
        </div>
      </>
    );
  }
}
