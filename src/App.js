import logo from "./logo.svg";
import "./App.css";
import ExerciseShoe from "./ExerciseShoesShop/ExerciseShoe";

function App() {
  return (
    <>
      <ExerciseShoe />
    </>
  );
}

export default App;
